<!--<!DOCTYPE html>-->
<style>
    .form-horizontal.style-form .form-group{
        padding-bottom: 2px !important;
        /*        margin-bottom: 0px !important;*/
    }
    .form-group{
        margin-bottom: 2px !important;
    }
    input:focus,textArea:focus{
        /*background-color: #FFFFB2;*/
        background-color: #FFFF66;
    }
    .mark-error{
        border-color:red !important; 
        background-color:#fdbbbb;
    }
    .control-label-custom
    {
        margin-bottom: 0;
        padding-top: 7px;
        text-align: right;
    }
    .control-label-data {
        margin-bottom: 0;
        font-weight: bold;
        font-size: 15px;
        padding-top: 2px;
    }

    .red .active a,
    .red .active a:hover {
        background-color: red;
    }
    .green-panel{
        background: none;
    }
    .pn {
        box-shadow: 0 1px 5px rgba(0, 0, 0, 0.2);
    }
    .pn:hover {
        box-shadow: 0 3px 5px rgba(0, 0, 0, 0.2);
    }
    .form-horizontal .form-group {
        margin-left: 0%;
        margin-right: 0%;
    }
    .label-ind{
        font-weight: bold;
        padding-right: 5px;
    }
    .label-ind-strong{
        font-weight: 900;
        padding-right: 5px;
    }
    .label-en{
        font-weight: bold;
        color: #EC971F;
        padding-right: 5px;
    }
    .label-italic{
        font-style: italic;
        padding-right: 5px;
    }
    .selected{
        background-color: yellow !important;
    }

    .mask-currency{
        text-align: right;
    }
    .no-gutter > [class*='col-'] {
        padding-right:0;
        padding-left:0;
    }
    .thead-inverse {
        background-color: #324D5C;
        color: #fff;
    }
    table.dataTable thead th, table.dataTable thead td {
        /*border-bottom: 1px solid #111;*/
        border-bottom: none !important;
        padding: 10px 18px;
    }
    table.dataTable tfoot td, table.dataTable.no-footer{
        border-bottom: 1px solid #ddd !important;
    }
    .dataTables_filter {
        display: none;
    }
    /*    .bigdrop {
            width: 300px !important;
        }*/

</style>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Dashboard">
        <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

        <title><?php echo Yii::app()->name; ?></title>
        <?php
        $baseThemeUrl = Yii::app()->theme->baseUrl;
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        ?>
        <link rel="shortcut icon" href="<?php echo Yii::app()->baseUrl; ?>/images/bps.ico">

        <!-- Bootstrap core CSS -->
        <link href="<?php echo $baseThemeUrl ?>/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo $baseThemeUrl ?>/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <!--<link href="<?php echo $baseThemeUrl ?>/js/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />-->
        <!--<link href="<?php echo $baseThemeUrl ?>/js/fancybox/jquery.fancybox.css" rel="stylesheet" />-->
        <!--external css-->
        <!--<link rel="stylesheet" type="text/css" href="<?php // echo $baseThemeUrl                                                                                                              ?>/css/zabuto_calendar.css">-->
        <!--<link rel="stylesheet" type="text/css" href="<?php // echo $baseThemeUrl                                                                                                              ?>/js/gritter/css/jquery.gritter.css" />-->
        <!--<link rel="stylesheet" type="text/css" href="<?php // echo $baseThemeUrl                                                                                                              ?>/lineicons/style.css">--> 
        <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl ?>/css/breadcrumbs.css"> 
        <link rel="stylesheet" type="text/css" href="<?php echo $baseThemeUrl ?>/css/oneui.css"> 
        <!-- Custom styles for this template -->
        <link href="<?php echo $baseThemeUrl ?>/css/style.css" rel="stylesheet">
        <link href="<?php echo $baseThemeUrl ?>/css/style-responsive.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl . '/js/jvector-map/jquery-jvectormap-2.0.3.css' ?>">

        <!--<script src="<?php echo $baseThemeUrl ?>/js/chart-master/Chart.js"></script>-->
    </head>

    <body class="skin-blue">
        <div class="header-navbar-fixed">
            <header id="header-navbar" class="content-mini content-mini-full" style="box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2);">
                <ul class="nav-header pull-right">
                    <li>
                        <button class="btn btn-rounded btn-default btn-login" type="button">Login <i class="fa fa-sign-in"></i>
                        </button>
                    </li>
                </ul>
                <ul class="nav-header pull-left">
                    <li class="hidden-md hidden-lg">
                    </li>
                    <li class="hidden-xs hidden-sm">
                        <h1>Sustainable Development Goals (SDGs)</h1>
                    </li>
                </ul>
            </header>
            <div id="main-container"  style="background-color: #F0F0F0">
                <div class="content" style="max-width: 95%;padding-top: 5px">
                    <div class="row">
                        <div class="wrapper row-offcanvas row-offcanvas-left">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="col-sm-2 col-lg-3"></div>
                                <div class="col-sm-8 col-lg-6">
                                    <img class="img-responsive" src="<?php echo $baseUrl ?>/images/sdgs_judul.png" alt="">
                                    <br>
                                </div>
                                <div class="col-sm-2 col-lg-3"></div>
                            </div>
                            <!--<h3>17 Tujuan Pembangunan Nasional</h3>-->
                            <div class="row" style="margin-left: 50px;margin-right: 50px">
                                <div class="col-sm-4 col-lg-2 animated fadeIn" style="padding: 0px">
                                    <a href="#sdgs" value="1" class="btn-sdgs">
                                        <div class="img-container fx-img-zoom-in fx-opt-slide-left">
                                            <img class="img-responsive" src="<?php echo $baseUrl ?>/images/1.png" alt="">
                                        </div> 
                                    </a>
                                </div>
                                <div class="col-sm-4 col-lg-2 animated fadeIn" style="padding: 0px">
                                    <a href="#sdgs" value="2" class="btn-sdgs">
                                        <div class="img-container fx-img-zoom-in fx-opt-slide-left">
                                            <img class="img-responsive" src="<?php echo $baseUrl ?>/images/2.png" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-4 col-lg-2 animated fadeIn" style="padding: 0px">
                                    <a href="#sdgs" value="3" class="btn-sdgs">
                                        <div class="img-container fx-img-zoom-in fx-opt-slide-left">
                                            <img class="img-responsive" src="<?php echo $baseUrl ?>/images/3.png" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-4 col-lg-2 animated fadeIn" style="padding: 0px">
                                    <a href="#sdgs" value="4" class="btn-sdgs">
                                        <div class="img-container fx-img-zoom-in fx-opt-slide-left">
                                            <img class="img-responsive" src="<?php echo $baseUrl ?>/images/4.png" alt="">
                                        </div>
                                    </a>
                                </div>   
                                <div class="col-sm-4 col-lg-2 animated fadeIn" style="padding: 0px">
                                    <a href="#sdgs" value="5" class="btn-sdgs">
                                        <div class="img-container fx-img-zoom-in fx-opt-slide-left">
                                            <img class="img-responsive" src="<?php echo $baseUrl ?>/images/5.png" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-4 col-lg-2 animated fadeIn" style="padding: 0px">
                                    <a href="#sdgs" value="6" class="btn-sdgs">
                                        <div class="img-container fx-img-zoom-in fx-opt-slide-left">
                                            <img class="img-responsive" src="<?php echo $baseUrl ?>/images/6.png" alt="">
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="row" style="margin-left: 50px;margin-right: 50px">
                                <div class="col-sm-4 col-lg-2 animated fadeIn" style="padding: 0px">
                                    <a href="#sdgs" value="7" class="btn-sdgs">
                                        <div class="img-container fx-img-zoom-in fx-opt-slide-left">
                                            <img class="img-responsive" src="<?php echo $baseUrl ?>/images/7.png" alt="">
                                        </div> 
                                    </a>
                                </div>
                                <div class="col-sm-4 col-lg-2 animated fadeIn" style="padding: 0px">
                                    <a href="#sdgs" value="8" class="btn-sdgs">
                                        <div class="img-container fx-img-zoom-in fx-opt-slide-left">
                                            <img class="img-responsive" src="<?php echo $baseUrl ?>/images/8.png" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-4 col-lg-2 animated fadeIn" style="padding: 0px">
                                    <a href="#sdgs" value="9" class="btn-sdgs">
                                        <div class="img-container fx-img-zoom-in fx-opt-slide-left">
                                            <img class="img-responsive" src="<?php echo $baseUrl ?>/images/9.png" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-4 col-lg-2 animated fadeIn" style="padding: 0px">
                                    <a href="#sdgs" value="10" class="btn-sdgs">
                                        <div class="img-container fx-img-zoom-in fx-opt-slide-left">
                                            <img class="img-responsive" src="<?php echo $baseUrl ?>/images/10.png" alt="">
                                        </div>
                                    </a>
                                </div>   
                                <div class="col-sm-4 col-lg-2 animated fadeIn" style="padding: 0px">
                                    <a href="#sdgs" value="11" class="btn-sdgs">
                                        <div class="img-container fx-img-zoom-in fx-opt-slide-left">
                                            <img class="img-responsive" src="<?php echo $baseUrl ?>/images/11.png" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-4 col-lg-2 animated fadeIn" style="padding: 0px">
                                    <a href="#sdgs" value="12" class="btn-sdgs">
                                        <div class="img-container fx-img-zoom-in fx-opt-slide-left">
                                            <img class="img-responsive" src="<?php echo $baseUrl ?>/images/12.png" alt="">
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="row" style="margin-left: 50px;margin-right: 50px">
                                <div class="col-sm-4 col-lg-2 animated fadeIn" style="padding: 0px">
                                    <a href="#sdgs" value="13" class="btn-sdgs">
                                        <div class="img-container fx-img-zoom-in fx-opt-slide-left">
                                            <img class="img-responsive" src="<?php echo $baseUrl ?>/images/13.png" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-4 col-lg-2 animated fadeIn" style="padding: 0px">
                                    <a href="#sdgs" value="14" class="btn-sdgs">
                                        <div class="img-container fx-img-zoom-in fx-opt-slide-left">
                                            <img class="img-responsive" src="<?php echo $baseUrl ?>/images/14.png" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-4 col-lg-2 animated fadeIn" style="padding: 0px">
                                    <a href="#sdgs" value="15" class="btn-sdgs">
                                        <div class="img-container fx-img-zoom-in fx-opt-slide-left">
                                            <img class="img-responsive" src="<?php echo $baseUrl ?>/images/15.png" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-4 col-lg-2 animated fadeIn" style="padding: 0px">
                                    <a href="#sdgs" value="16" class="btn-sdgs">
                                        <div class="img-container fx-img-zoom-in fx-opt-slide-left">
                                            <img class="img-responsive" src="<?php echo $baseUrl ?>/images/16.png" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-4 col-lg-2 animated fadeIn" style="padding: 0px">
                                    <a href="#sdgs" value="17" class="btn-sdgs">
                                        <div class="img-container fx-img-zoom-in fx-opt-slide-left">
                                            <img class="img-responsive" src="<?php echo $baseUrl ?>/images/17.png" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-4 col-lg-2 animated fadeIn" style="padding: 0px">
                                    <a href="javascript:void(0)" value="18" class="btn-sdgs">
                                        <div class="img-container fx-img-zoom-in fx-opt-slide-left">
                                            <img class="img-responsive" src="<?php echo $baseUrl ?>/images/18.png" alt="">
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="sdgs">
                    <br><br><br><br>
                    <div class="block block-themed" style="max-width: 90%; background-color: white;margin-left: 5%;margin-right: 5%">
                        <div class="block-header bg-flat" id="judul-col">
                            <div id="judul-tujuan">
                                <h2 style="color: white">TABEL DAN GRAFIK</h2>
                            </div>
                        </div>
                        <div class="block-content" style="padding-top: 0px">
                            <div class="content">
                                <div class="row">
                                    <div class="form-group push-10-t">
                                        <!--<div class="col-sm-1 col-md-1 col-lg-1"></div>-->
                                        <div class="col-sm-4 col-md-4 col-lg-4">
                                            <div class="form-material">
                                                <?php echo CHtml::dropDownList('tujuan', '', $tujuan, array('class' => 'js-select2 form-control', 'style' => 'width: 100%')) ?>
                                                <label style="font-size: 14px">Tujuan</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-md-2 col-lg-2"></div>
                                        <div class="col-sm-4 col-md-4 col-lg-4">
                                            <div class="form-material">
                                                <?php echo CHtml::dropDownList('indikator', '', $indikator, array('class' => 'form-control', 'style' => 'width: 100%')) ?>
                                                <label style="font-size: 14px">Indikator</label>
                                            </div>
                                        </div>
                                        <!--                                        <div class="col-sm-4 col-md-4 col-lg-4" style="margin-bottom: 10px">
                                                                                    <div class="form-material">
                                        <?php // echo CHtml::dropDownList('tahun', '', $tahun, array('class' => 'form-control')) ?>
                                                                                        <label style="font-size: 14px">Tahun</label>
                                                                                    </div>
                                                                                </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block" style="max-width: 90%; background-color: white;margin-left: 5%;margin-right: 5%">
                        <ul class="nav nav-tabs nav-justified" data-toggle="tabs" style="font-size: 14px;background-color: #EBEBEB">
                            <li class="active">
                                <a data-toggle="tab" href="#grafik" id="tab_grafik"><i class="fa fa-bar-chart"></i> Grafik</a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#tabel" id="tab_tabel"><i class="fa fa-table"></i> Tabel</a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#peta" id="tab_peta"><i class="fa fa-map-marker"></i> Peta</a>
                            </li>
                        </ul>
                        <div class="block-content tab-content">
                            <div class="tab-pane fade fade-right active in" id="grafik">
                                <br>
                                <h3 style="text-align: center" id="judul-indikator"></h3><br>
                                <div class="row" style="min-height: 550px">
                                    <div class="col-sm-12 col-md-12 col-lg-12 text-center" id="loader" style="display:none;">
                                        <i class="fa fa-spinner fa-spin fa-2x fa-fw margin-bottom" style="font-size: 100px;"></i>
                                    </div>
                                    <div class="col-sm-12 col-md-5 col-lg-5">
                                        <div class="block-content text-center" id="chart2">

                                        </div>
                                        <div class="block-content text-center" id="chart3">

                                        </div>
                                        <div class="block-content text-center" id="chart4">
                                            <br><br>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-7 col-lg-7">
                                        <div class="block-content text-center" id="chart1">

                                        </div>
                                    </div>
                                </div>
                                <!--                                <h3 style="text-align: center;margin-top:20px" id="judul-indikator3"></h3><br>
                                                                <div class="row" style="min-height: 550px">
                                                                    <div class="col-md-4 text-center" id="select_tahun" style="display: none;float: right">
                                <?php // echo CHtml::dropDownList('tahun', '', $tahun, array('class' => 'form-control', 'style' => 'visible:false')) ?></div>
                                                                    <div class="col-md-12 text-center" id="loader3" style="display:none;">
                                                                        <i class="fa fa-spinner fa-spin fa-2x fa-fw margin-bottom" style="font-size: 100px;"></i>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div class="col-sm-12 col-sm-12" id="map-sdgs" style="height: 450px;">
                                                                                                                        <div style="text-align: center">
                                                                                                                            <i class="fa fa-spinner fa-spin fa-2x fa-fw margin-bottom" style="font-size: 100px;" id="loader"></i>
                                                                                                                        </div>
                                                                        </div>
                                                                    </div>
                                                                </div>-->
                                <!--</div>-->
                            </div>
                            <div class="tab-pane fade fade-right" id="tabel">
                                <a download="<?php echo 'Tabel_SDGs_' . DateTimeHelper::CurrentDate() . '.xls' ?>" href="javascript:void(0)" 
                                   class="btn btn-success" 
                                   onclick="return ExcellentExport.excel(this, 'data-table-sdgs', 'rekap');"
                                   style="float:right">
                                    <i class="fa fa-download"></i> Download Excel
                                </a>
                                <br><br><br>
                                <div id="tab-batas-table"></div>
                                <div id='data-table-sdgs'>
                                    <br><h3 style="text-align: center" id="judul-indikator2"></h3><br>
                                    <div class="row" style="min-height: 700px">
                                        <div class="col-sm-12 col-md-12 col-lg-12" id="dataTable">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade fade-right in" id="peta">
                                <br>
                                <h3 style="text-align: center;margin-top:20px" id="judul-indikator3"></h3><br>
                                <div class="row" style="min-height: 550px">
                                    <div class="col-md-4 text-center" id="select_tahun" style="display: none;float: right">
                                        <?php echo CHtml::dropDownList('tahun', '', $tahun, array('class' => 'form-control', 'style' => 'visible:false')) ?></div>
                                    <div class="col-md-12 text-center" id="loader3" style="display:none;">
                                        <i class="fa fa-spinner fa-spin fa-2x fa-fw margin-bottom" style="font-size: 100px;"></i>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-sm-12 col-sm-12" id="map-sdgs" style="height: 450px;">
                                            <!--                                            <div style="text-align: center">
                                                                                            <i class="fa fa-spinner fa-spin fa-2x fa-fw margin-bottom" style="font-size: 100px;" id="loader"></i>
                                                                                        </div>-->
                                        </div>
                                    </div>
                                </div>
                                <!--</div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/jQuery-2.1.4.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/jQuery-migrate-1.2.1.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/bootstrap.min.js"></script>
        <script class="include" type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/jquery.scrollTo.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/counter.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/highcharts/highcharts.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/highcharts/modules/exporting.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/highcharts/themes/sand-signika.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/jvector-map/jquery-jvectormap-2.0.3.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/jvector-map/indonesia-provinces-bps.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/excellentexport.min.js"></script>
        <!--common script for all pages-->
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/common-scripts.js"></script>

        <script type="text/javascript">
                                       $(document).ready(function () {
                                           $('.btn-login').live('click', function () {
                                               window.location = '<?php echo Yii::app()->createUrl('site/login') ?>';
                                           });

                                           $('#tujuan').live('change', function () {
                                               var tujuan = $(this).val();
                                               var text = $('#tujuan option:selected').text();
                                               $('#judul-tujuan').html("<h2 style='color: white'>TABEL DAN GRAFIK " + text + "</h2>");
                                               $('#indikator').html('<option selected="selected" value="">---PILIH INDIKATOR---</option>');
                                               $('#tahun').html('<option selected="selected" value="">---PILIH TAHUN---</option>');
                                               if (tujuan !== '') {
                                                   $.ajax({
                                                       url: '<?php echo Yii::app()->createUrl('helper/setIndikatorDashboard') ?>',
                                                       type: 'post',
                                                       dataType: 'html',
                                                       data: {ajax: true, tujuan: tujuan},
                                                       success: function (html) {
                                                           $('#indikator').html(html);
                                                       }
                                                   });
                                               }
                                           });

                                           $('#indikator').live('change', function () {
                                               var tujuan = $('#tujuan').val();
                                               var indikator = $(this).val();
                                               $('#tahun').html('<option selected="selected" value="">---PILIH TAHUN---</option>');
                                               if (tujuan !== '' && indikator !== '') {
                                                   $('#loader').show();
                                                   $.ajax({
                                                       url: '<?php echo Yii::app()->createUrl('helper/setAvailableTahun') ?>',
                                                       type: 'post',
                                                       dataType: 'html',
                                                       data: {ajax: true, tujuan: tujuan, indikator: indikator},
                                                       success: function (html) {
                                                           $('#tahun').html(html);
                                                           $("#tahun").prop("selectedIndex", 1);
                                                           setTimeout(function () {
                                                               generateJudul(indikator);
                                                               generateChart(indikator);
                                                               generateTable(indikator);
                                                               generateMaps(indikator, $("#tahun").val());
                                                           }, 100);
                                                       }
                                                   });
                                               }
                                           });

                                           $('#tahun').live('change', function () {
                                               var tujuan = $('#tujuan').val();
                                               var indikator = $('#indikator').val();
                                               var tahun = $('#tahun').val();
                                               if (tujuan !== '' && indikator !== '' && tahun !== '') {
                                                   setTimeout(function () {
                                                       generateMaps(indikator, tahun);
                                                   }, 100);
                                               }
                                           });

                                           $('.btn-sdgs').live('click', function () {
                                               var val = $(this).attr('value');
                                               $('#tujuan').val(val);
                                               $('#tujuan').trigger('change');
                                           });

                                           function generateJudul(indikator) {
                                               $.ajax({
                                                   url: '<?php echo Yii::app()->createUrl('helper/getJudulIndikator') ?>',
                                                   type: 'post',
                                                   dataType: 'json',
                                                   data: {ajax: true, indikator: indikator},
                                                   success: function (data) {
                                                       $('#judul-indikator').html(data.judul);
                                                       $('#judul-indikator2').html(data.judul);
                                                       $('#judul-indikator3').html(data.judul);
                                                   }
                                               });
                                           }

                                           function generateAgregat(tujuan, agregat, tahun) {
                                               $.ajax({
                                                   url: '<?php echo Yii::app()->createUrl('helper/getJudulAgregat') ?>',
                                                   type: 'post',
                                                   dataType: 'json',
                                                   data: {ajax: true, agregat: agregat},
                                                   success: function (data) {
                                                       $('#judul-agregat2').html(data.judul + ' ' + tahun);
                                                   }
                                               });
                                           }

                                           function generateTable(indikator) {
                                               $.ajax({
                                                   url: '<?php echo Yii::app()->createUrl('site/getDataTable') ?>',
                                                   type: 'post',
                                                   dataType: 'json',
                                                   data: {ajax: true, indikator: indikator},
                                                   success: function (data) {
                                                       $('#dataTable').html(data.tabel);
                                                   }
                                               });
                                           }

                                           function generateChart(indikator) {
                                               $.ajax({
                                                   url: '<?php echo Yii::app()->createUrl('site/getDataChart1') ?>',
                                                   type: 'post',
                                                   dataType: 'json',
                                                   data: {ajax: true, indikator: indikator},
                                                   success: function (data) {
                                                       if (data !== '') {
                                                           var series = [];
                                                           var objdata = data.value;
                                                           for (var key in objdata) {
                                                               var targetSeries;
                                                               var obj = objdata[key];
                                                               targetSeries = {'name': key, 'data': obj};
                                                               series.push(targetSeries);
                                                           }
                                                           Highcharts.chart('chart1', {
                                                               chart: {
                                                                   type: 'bar',
                                                                   height: 1200,
                                                               },
                                                               title: {
                                                                   text: 'Angka Provinsi'
                                                               },
                                                               xAxis: {
                                                                   categories: data.nama,
                                                                   title: {
                                                                       text: ''
                                                                   }
                                                               },
                                                               yAxis: {
                                                                   min: 0,
                                                                   title: {
                                                                       text: '',
                                                                       align: 'high'
                                                                   },
                                                                   labels: {
                                                                       overflow: 'justify'
                                                                   }
                                                               },
                                                               tooltip: {
                                                                   valueSuffix: ''
                                                               },
                                                               plotOptions: {
                                                                            series: {
                                                                                dataLabels: {
                                                                                    enabled: true,
                                                                                    align: 'right',
                                                                                    color: '#FFFFFF',
                                                                                    x: -10
                                                                                },
                                                                                pointPadding: 0.15,
                                                                                groupPadding: 0
                                                                            },
                                                                            bar:{  
                                                                                dataLabels:{  
                                                                                   padding:0,
                                                                                   allowOverlap:true
                                                                                   
                                                                                }
                                                                            }
                                                                        },
                                                               credits: {
                                                                   enabled: false
                                                               },
                                                               series: series,
                                                               loading: {
                                                                   labelStyle: {
                                                                       backgroundImage: 'url("http://jsfiddle.net/img/logo.png")',
                                                                       display: 'block',
                                                                       width: '136px',
                                                                       height: '26px',
                                                                       backgroundColor: '#000'
                                                                   }
                                                               },
                                                               error: function () {
                                                                   $('#chart1').html('<h3>DATA TIDAK DITEMUKAN</h3>');
                                                               }
                                                           });
                                                       }
                                                   }
                                               });
                                               $.ajax({
                                                   url: '<?php echo Yii::app()->createUrl('site/getDataChart2') ?>',
                                                   type: 'post',
                                                   dataType: 'json',
                                                   data: {ajax: true, indikator: indikator},
                                                   success: function (data) {
                                                       Highcharts.chart('chart2', {
                                                           chart: {
                                                               zoomType: 'xy'
                                                           },
                                                           title: {
                                                               text: 'Angka Nasional'
                                                           },
                                                           xAxis: {
                                                               categories: data.nama,
                                                               crosshair: true
                                                           },
                                                           yAxis: [{// Primary yAxis
                                                                   min: 0,
                                                                   labels: {
                                                                       style: {
                                                                           color: Highcharts.getOptions().colors[1]
                                                                       }
                                                                   },
                                                                   title: {
                                                                       text: '',
                                                                       style: {
                                                                           color: Highcharts.getOptions().colors[1]
                                                                       }
                                                                   },
                                                                   allowDecimals: true,
                                                               }, {// Secondary yAxis
                                                                   min: 0,
                                                                   title: {
                                                                       text: '',
                                                                       style: {
                                                                           color: Highcharts.getOptions().colors[0]
                                                                       }
                                                                   },
                                                                   labels: {
                                                                       style: {
                                                                           color: Highcharts.getOptions().colors[0]
                                                                       }
                                                                   },
                                                                   allowDecimals: true,
                                                                   opposite: true
                                                               }],
                                                           tooltip: {
                                                               formatter: function () {
                                                                   return '<b>' + this.x + '</b><br/>' +
                                                                           this.series.name + ': ' + this.y;
                                                               }
                                                           },
                                                           credits: {
                                                               enabled: false
                                                           },
                                                           series: [{
                                                                   name: 'Indonesia',
                                                                   type: 'column',
                                                                   yAxis: 1,
                                                                   data: data.value,
                                                               }, {
                                                                   name: 'Indonesia',
                                                                   type: 'spline',
                                                                   data: data.value,
                                                               }],
                                                           loading: {
                                                               labelStyle: {
                                                                   backgroundImage: 'url("http://jsfiddle.net/img/logo.png")',
                                                                   display: 'block',
                                                                   width: '136px',
                                                                   height: '26px',
                                                                   backgroundColor: '#000'
                                                               }
                                                           },
                                                           error: function () {
                                                               $('#chart2').html('<h3>DATA TIDAK DITEMUKAN</h3>');
                                                           }
                                                       });
                                                   }
                                               });
                                               $.ajax({
                                                   url: '<?php echo Yii::app()->createUrl('site/getDataChart3') ?>',
                                                   type: 'post',
                                                   dataType: 'json',
                                                   data: {ajax: true, indikator: indikator},
                                                   success: function (data) {
                                                       if (data !== '') {
                                                           var series = [];
                                                           var objdata = data.value;
                                                           for (var key in objdata) {
                                                               var targetSeries;
                                                               var obj = objdata[key];
                                                               targetSeries = {'name': key, 'data': obj};
                                                               series.push(targetSeries);
                                                           }
                                                           Highcharts.chart('chart3', {
                                                               chart: {
                                                                   type: 'column',
                                                                   height: 400,
                                                               },
                                                               title: {
                                                                   text: 'Daerah Tempat Tinggal'
                                                               },
                                                               xAxis: {
                                                                   categories: data.nama
                                                               },
                                                               yAxis: {
                                                                   allowDecimals: false,
                                                                   min: 0,
                                                                   title: {
                                                                       text: ''
                                                                   }
                                                               },
                                                               tooltip: {
                                                                   formatter: function () {
                                                                       return '<b>' + this.x + '</b><br/>' +
                                                                               this.series.name + ': ' + this.y;
                                                                   }
                                                               },
                                                               credits: {
                                                                   enabled: false
                                                               },
                                                               series: series,
                                                               loading: {
                                                                   labelStyle: {
                                                                       backgroundImage: 'url("http://jsfiddle.net/img/logo.png")',
                                                                       display: 'block',
                                                                       width: '136px',
                                                                       height: '26px',
                                                                       backgroundColor: '#000'
                                                                   }
                                                               },
                                                               error: function () {
                                                                   $('#chart3').html('<h3>DATA TIDAK DITEMUKAN</h3>');
                                                               }
                                                           });
                                                       }
                                                   }
                                               });
                                               $.ajax({
                                                   url: '<?php echo Yii::app()->createUrl('site/getDataChart4') ?>',
                                                   type: 'post',
                                                   dataType: 'json',
                                                   data: {ajax: true, indikator: indikator},
                                                   success: function (data) {
                                                       if (data !== '') {
                                                           var series = [];
                                                           var objdata = data.value;
                                                           for (var key in objdata) {
                                                               var targetSeries;
                                                               var obj = objdata[key];
                                                               targetSeries = {'name': key, 'data': obj};
                                                               series.push(targetSeries);
                                                           }
                                                           Highcharts.chart('chart4', {
                                                               chart: {
                                                                   type: 'column',
                                                                   height: 400,
                                                               },
                                                               title: {
                                                                   text: 'Jenis Kelamin'
                                                               },
                                                               xAxis: {
                                                                   categories: data.nama
                                                               },
                                                               yAxis: {
                                                                   allowDecimals: false,
                                                                   min: 0,
                                                                   title: {
                                                                       text: ''
                                                                   }
                                                               },
                                                               tooltip: {
                                                                   formatter: function () {
                                                                       return '<b>' + this.x + '</b><br/>' +
                                                                               this.series.name + ': ' + this.y;
                                                                   }
                                                               },
                                                               credits: {
                                                                   enabled: false
                                                               },
                                                               series: series,
                                                               loading: {
                                                                   labelStyle: {
                                                                       backgroundImage: 'url("http://jsfiddle.net/img/logo.png")',
                                                                       display: 'block',
                                                                       width: '136px',
                                                                       height: '26px',
                                                                       backgroundColor: '#000'
                                                                   }
                                                               },
                                                               error: function () {
                                                                   $('#chart4').html('<h3>DATA TIDAK DITEMUKAN</h3>');
                                                               }
                                                           });
                                                       }
                                                   }
                                               });
                                               $('#loader').hide();
                                           }

                                           function generateMaps(indikator, tahun) {
                                               $('#select_tahun').show();
                                               var valueColor = new Array();
                                               var valueData = new Array();
                                               var valueLegend = new Array();
                                               var valueLabel = new Array();
                                               var scalemap = {
                                                   '1': '#FFA07A',
                                                   '2': '#F08080',
                                                   '3': '#BB5555',
                                                   '4': '#8F1414'
                                               };
                                               $.ajax({
                                                   url: '<?php echo Yii::app()->createUrl('site/getDataMaps') ?>',
                                                   type: 'post',
                                                   dataType: 'json',
                                                   data: {ajax: true, indikator: indikator, tahun: tahun},
                                                   success: function (data) {
                                                       if (data.status && data.data != '') {
                                                           valueColor = data.color;
                                                           valueData = data.data;
                                                           valueLegend = data.legend;
                                                           valueLabel = data.label;
                                                           $('#map-sdgs').html('');
                                                           $('#map-sdgs').vectorMap({
                                                               map: 'Indonesia_mill_en',
                                                               backgroundColor: "transparent",
                                                               regionStyle: {
                                                                   initial: {
                                                                       fill: '#DADAD2',
                                                                       "fill-opacity": 1,
                                                                       stroke: 'none',
                                                                       "stroke-width": 0,
                                                                       "stroke-opacity": 1
                                                                   }
                                                               },
                                                               regionLabelStyle: {
                                                                   initial: {
                                                                       fill: '#575555',
                                                                       "font-size": "9px",
                                                                   },
                                                                   hover: {
                                                                       fill: 'black'
                                                                   }
                                                               },
                                                               labels: {
                                                                   regions: {
                                                                       render: function (code) {
                                                                           return valueLabel[code] + ' : ' + valueData[code];
                                                                       }
                                                                   }
                                                               },
                                                               series: {
                                                                   regions: [{
                                                                           values: valueColor,
                                                                           scale: scalemap,
                                                                           attribute: 'fill',
                                                                           legend: {
                                                                               vertical: true,
                                                                               //cssClass: 'jvectormap-legend-bg',
                                                                               title: '',
                                                                               labelRender: function (v) {
                                                                                   return {
                                                                                       1: valueLegend[1],
                                                                                       2: valueLegend[2],
                                                                                       3: valueLegend[3],
                                                                                       4: valueLegend[4],
                                                                                   }[v];
                                                                               }
                                                                           },
                                                                           normalizeFunction: 'polynomial'
                                                                       }],
                                                               },
                                                               onRegionTipShow: function (e, el, code) {
                                                                   if (typeof valueData[code] != "undefined")
                                                                       el.html(el.html() + '<br>Nilai : ' + valueData[code]);
                                                               },
                                                           });
                                                       } else {
                                                           $('#map-sdgs').html('');
                                                       }
                                                   }
                                               });
                                           }

                                           $('#tab_grafik').click(function () {
//                                               $('#loader').show();
//                                               chart1.destroy();
//                                               chart2.destroy();
//                                               chart3.destroy();
//                                               chart4.destroy();
//                                               var tujuan = $('#tujuan').val();
//                                               var indikator = $('#indikator').val();
//                                               if (tujuan !== '' && indikator !== '') {
//                                                   generateChart(indikator);
//                                               }
//                                               $('#loader').hide();
                                               setTimeout(function () {
                                                   $(Highcharts.charts).each(function (i, chart) {
                                                       chart.reflow();
                                                   });
                                               }, 400);
                                           });

                                           $('#tab_peta').click(function () {
                                               $('#loader3').show();
                                               var tujuan = $('#tujuan').val();
                                               var indikator = $('#indikator').val();
                                               if (tujuan !== '' && indikator !== '') {
                                                   $.ajax({
                                                       url: '<?php echo Yii::app()->createUrl('helper/setAvailableTahun') ?>',
                                                       type: 'post',
                                                       dataType: 'html',
                                                       data: {ajax: true, tujuan: tujuan, indikator: indikator},
                                                       success: function (html) {
                                                           $('#tahun').html(html);
                                                           $("#tahun").prop("selectedIndex", 1);
                                                           setTimeout(function () {
                                                               generateJudul(indikator);
                                                               generateMaps(indikator, $("#tahun").val());
                                                           }, 100);
                                                       }
                                                   });
                                               }
                                               $('#loader3').hide();
                                           });
                                       });
        </script>
    </body>

</html>


