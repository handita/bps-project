"""mysite crud Configuration

The `crudpatterns` list routes cruds to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/cruds/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a crud to crudpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a crud to crudpatterns:  path('', Home.as_view(), name='home')
Including another crudconf
    1. Import the include() function: from django.cruds import include, path
    2. Add a crud to crudpatterns:  path('blog/', include('blog.cruds'))
"""
from django.contrib import admin
from django.cruds import path,include

crudpatterns = [
    path('admin/', admin.site.cruds),
    path('polls/', include('polls.cruds')),
    path('asset/', include('asset.cruds'))
]
